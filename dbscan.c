#include<stdio.h>
#include<math.h>

void test_quantidade_vizinhos_proximos();
void test_cria_core();
int quantos_vizinhos(float raio, int dado, int quantidade_de_dados, int minVizinhos);
void test_quantidade_vizinhos_proximos();
void cria_core(float raio, int minVizinhos, int quantidade_de_dados);
float distancia_euclidiana(float x1,float y1,float x2,float y2);
void agrupa_dados(float raio,int quantidade_de_dados,int dado,int cluster);
void teste_dbscan();


//        0  , 1 , 2    , 3       4
// matriz[ x , y , tipo , cluster, ja_visitado]
// tipo : 0 - n classificado; 1- borda; 2- core; 3 - ruido
float matriz[10][5] = {{5,5,0,0,0},{5,10,0,0,0},{7.5,7.5,0,0,0},{10,10,0,0,0},{10,5,0,0,0},
                      {20,20,0,0,0},{20,25,0,0,0},{22.5,22.5,0,0,0},{25,25,0,0,0},{25,20,0,0,0}};


int main(){
  //test_quantidade_vizinhos_proximos();
  //test_cria_core();
  teste_dbscan();

  return 0;
}

void dbscan(float raio, int minVizinhos, int quantidade_de_dados){

  //acha todos os cores
  int i;
  int cluster = 1;

  for(i =0;i<quantidade_de_dados;i++){
    //acha os cores
    if((matriz[i][2]==2)&&(matriz[i][4]==0)){
      matriz[i][3]=cluster;
      matriz[i][4]=1;
      agrupa_dados(raio,quantidade_de_dados,i,cluster);
      cluster ++;
    }
  }

}

void agrupa_dados(float raio,int quantidade_de_dados,int dado,int cluster){

  int i;
  float x1,x2,y1,y2;


  for(i =0;i<dado;i++){
    // acha os dados e ve se estao em seu raio
    if(matriz[i][4]==0){
      x1 = matriz[i][0];
      x2 = matriz[dado][0];
      y1 = matriz[i][1];
      y2 = matriz[dado][1];

      // os dados que entrarem aqui vao ser do clusters
      // porem os mesmos podem ser tanto cores como bordas
      if(distancia_euclidiana(x1,y1,x2,y2) <= raio){
        // se sao cores assimila e chama recursivamente
        if(matriz[i][2]==2){
          matriz[i][4] = 1;
          matriz[i][3] = cluster;
          agrupa_dados(raio,quantidade_de_dados,i,cluster);
        }// se nao so assimila
        else{
          matriz[i][3] = cluster;
          matriz[i][2] = 1;
        }
      }
    }
  }

  for(i = (quantidade_de_dados-1) ;i>dado;i--){

    // acha os dados e ve se estao em seu raio
    if(matriz[i][4]==0){
      x1 = matriz[i][0];
      x2 = matriz[dado][0];
      y1 = matriz[i][1];
      y2 = matriz[dado][1];

      // os dados que entrarem aqui vao ser do clusters
      // porem os mesmos podem ser tanto cores como bordas
      if(distancia_euclidiana(x1,y1,x2,y2) <= raio){
        // se sao cores assimila e chama recursivamente
        if(matriz[i][2]==2){
          matriz[i][4] = 1;
          matriz[i][3] = cluster;
          agrupa_dados(raio,quantidade_de_dados,i,cluster);
        }// se nao so assimila
        else{
          matriz[i][3] = cluster;
          matriz[i][2] = 1;
        }
      }
    }
  }


}
// funcao que acha os cores ruidos e candidados a borda
void cria_core(float raio, int minVizinhos, int quantidade_de_dados){
  // no algoritimo classico ele come;a do random e vai achando,
  // no meu ele come;a do meio e vai achando

  int i ;
  int aux;

  for(i = 0; i< (quantidade_de_dados/2);i++){

    aux = quantos_vizinhos(raio,i,quantidade_de_dados,minVizinhos);

    if(aux >= minVizinhos){
      matriz[i][2] = 2;
    }
    else if(aux == 0){
      matriz[i][2] = 3;
    }
    else{
      // este dado pode ser tanto borda como ru[ido]
      matriz[i][2] = 0;
    }
  }

  for(i = (quantidade_de_dados-1); i>(quantidade_de_dados/2) ;i--){

        aux = quantos_vizinhos(raio,i,quantidade_de_dados,minVizinhos);

        if(aux >= minVizinhos){
          matriz[i][2] = 2;
        }
        else if(aux == 0){
          matriz[i][2] = 3;
        }
        else{
          // este dado pode ser tanto borda como ru[ido]
          matriz[i][2] = 0;
        }
      }
}

int quantos_vizinhos(float raio, int dado, int quantidade_de_dados, int minVizinhos){

  int quantidade = 0;
  int i;
  float x1,x2,y1,y2;

  // o calculo dos vizinhos será realizado em dois passos
  // passo 1 : numeros menores que o dado a ser analizado
  // passo 2 : numeros maiores

  // passo 1
  for(i = 0;i<dado;i++){
    // distancia euclidiana
    x1 = matriz[i][0];
    x2 = matriz[dado][0];
    y1 = matriz[i][1];
    y2 = matriz[dado][1];

    if(distancia_euclidiana(x1,y1,x2,y2) <= raio)
      quantidade ++;

      // uma modificação é retornar quantidade já neste ponto caso quantidade
      // seja maior que minVizinhos

  }

  //passo 2
  for(i = (quantidade_de_dados-1);i>dado;i--){
    // distancia euclidiana
    x1 = matriz[i][0];
    x2 = matriz[dado][0];
    y1 = matriz[i][1];
    y2 = matriz[dado][1];

    if(distancia_euclidiana(x1,y1,x2,y2) <= raio)
      quantidade ++;
  }

  return quantidade;
}

float distancia_euclidiana(float x1,float y1,float x2,float y2){

  float distancia;

  distancia = ((x2 - x1)*(x2 - x1)) + ((y2-y1)*(y2-y1));
  distancia = sqrtf(distancia);

  return distancia;
}

void test_quantidade_vizinhos_proximos(){

int i = 0;
int aux;
//float matriz[10][2] = {{5,5},{5,10},{7.5,7.5},{10,10},{10,5},{20,20},{20,25},{22.5,22.5},{25,25},{25,20}}
  for(i;i<10;i++){
    aux = quantos_vizinhos(5,i,10,3);
    printf("\n posicao:%d quantos_vizinhos:%d",i,aux );
  }

}

void test_cria_core(){
  int i;

  cria_core(5,4,10);

  for(i = 0;i<10;i++){
    printf("\n matriz[%d][2]:%f \n",i,matriz[i][2]);
  }
}

void teste_dbscan(){

  int i;
  cria_core(5,4,10);
  dbscan(5,4,10);

  for(i = 0;i<10;i++){
    printf("\n matriz[%d][3]:%f \n",i,matriz[i][3]);
  }

}
