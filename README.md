# myDBSCAN

### Criado por Gabriel Lopes

### gabriel18.lopes@gmail.com

## Implementação do algoritimo de clustering DBSCAN em c

### Melhorias que poderiam ser implementadas
- [] look up table
- [] indexação espacial
- [] definir quanidade de dados como uma constante
- [] matriz dinamica

### Algoritmo clássico passo a passo
[youtube](https://www.youtube.com/watch?v=6jl9KkmgDIw)
 1. Se define a distancia do raio e o número de vizinhos
 2. Seleciona um dado aleatório e verifica quantos vizinhos próximos ele possui
 3. Caso tenha o número mínimo de vizinhos ele será considerado core
 4. Verifica os vizinhos, caso eles tenham número mínimo são considerados core, caso contrario são considerados borda
 5. Depois de realizar isso para todos os dados ele liga todos os core que são vizinhos e junta as bordas para formar os clusters

### Meu algoritimo

*  Diferente do algoritimo clássico este começa da posição um e passa por todos
1. Verifica quantos vizinhos o dado observado tem
2. Se possuir o número mínimo é considerado core
3. Apartir dos cores são encontrados os dados que se conectam a ele, tanto como vizinhos quanto como outros cores, assim são formados os clusters.

## Possibilidades de paralelismo
